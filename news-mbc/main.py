﻿#!/usr/bin/env python
# coding=UTF-8

import cgi
import datetime
import wsgiref.handlers
import os
import logging

from google.appengine.ext import blobstore, db, webapp
from google.appengine.ext.blobstore import BlobInfo
from google.appengine.api import users
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext.webapp import template
 
from django.utils import simplejson
from django.utils.html import strip_tags 

from dateutil import parser
from urlparse import urlparse
import urllib


class NewsItem(db.Model):
	topic_id = db.IntegerProperty()
	url = db.StringProperty()
	title = db.StringProperty()
	description = db.TextProperty()
	content = db.TextProperty()
	publish_date = db.DateTimeProperty()
	sync_date = db.DateTimeProperty(auto_now_add=True)
	source = db.StringProperty()

class Image(db.Model):
	url = db.StringProperty()
	blob_key = db.StringProperty()
	file_name = db.StringProperty()
	news_item = db.ReferenceProperty(reference_class = NewsItem, required = True, collection_name = 'images')
	
class MainPage(webapp.RequestHandler):
	def get(self):
		self.response.out.write('Welcome to MBC news gateway!')

class PostNews(webapp.RequestHandler):
	def post(self):
		if 'url' in self.request.arguments():
			items = NewsItem.all().filter("url =",self.request.get('url')).fetch(1)
			def update_news():
				values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
				url = self.request.get('url')
				item = NewsItem()
				item.topic_id = int (values.get('topic_id')) #Xa hoi
				item.url = url
				item.title = values.get('title')
				item.description = db.Text(values.get('description'))
				item.content = db.Text(values.get('content'))
				item.source = values.get('source')
				item.publish_date = parser.parse(self.request.get('publish_date')) if 'publish_date' in values else None
				item.put()
				self.response.out.write(url)
			
			if not items: # there is no such URL in data store yet
				db.run_in_transaction(update_news)
			else:
				self.response.out.write('Url %s is already in data store' % self.request.get('url'))
				
		else:
			self.response.out.write('No url posted.')
	def get(self):
		self.response.out.write('No item posted.')

class Topics(webapp.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'application/json'
		topic_list = [u'Trang Nhất', u'Xã Hội', u'Văn Hóa', u'Kinh Tế', u'Công Nghệ', u'Thể Thao', u'Giải Trí', u'Pháp Luật', u'Ô Tô Xe Máy', u'Thế Giới']
		json_data = [{'id':topic_list.index(topic)+1,'topic': topic} for topic in topic_list]
		self.response.out.write(simplejson.dumps(json_data))

class NewsHeader(webapp.RequestHandler):
	def get_readable_time(self,time):
		result = time.strftime('%d/%m/%Y')
		return result
		
	def get(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		topic_id = int(values.get('topic_id',0))
		offset = int(values.get('offset',0))
		limit = int(values.get('limit',10))
		if limit > 20:
			limit = 20
		elif limit < 0:
			limit = 10
		if offset < 0:
			offset = 0
		
		json_data = []
		if topic_id == 0 :
			for i in range (1,10):
				item = NewsItem.all().filter("topic_id =",i).order("-sync_date").fetch(1)
				if item:
					json_data.extend([{'news_id':item[0].key().id(),
							'news_title':item[0].title,
							'news_description':item[0].description,
							'news_date': self.get_readable_time(item[0].publish_date)
							} ])
		else:				
			items = NewsItem.all().filter("topic_id =",topic_id).order("-sync_date").fetch(offset=offset,limit=limit)
			json_data = [{'news_id':item.key().id(),
					'news_title':item.title,
					'news_description':item.description,
					'news_date': self.get_readable_time(item.publish_date)} for item in items]
		
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(simplejson.dumps(json_data))

class Test(webapp.RequestHandler):
	def get(self):
		self.response.out.write(self.get_readable_time(datetime.datetime.now()))
		
	def get_readable_time(self,time):
		result = time.strftime('%d/%m/%Y')
		return result

class NewsContent(webapp.RequestHandler):
	def get(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		id = int(values.get('news_id'))
		item = NewsItem.get_by_id(id)
		
		json_data = {'news_id':id,
					'news_title':item.title,
					'news_content':item.content,
					'news_image_url':[self.get_download_path(image.file_name) for image in item.images],
					'news_source': item.source,
					'news_date': self.get_readable_time(item.publish_date)
					} 
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(simplejson.dumps(json_data))
	
	def get_readable_time(self,time):
		result = time.strftime('%d/%m/%Y')
		return result
	
	def get_download_path(self, file_name):
		#Get download path for image file from blob key
		url_object = urlparse(self.request.url)
		host = url_object.scheme + "://" + url_object.netloc
		return os.path.join(host,"images/%s" % file_name)
		
class Images(blobstore_handlers.BlobstoreUploadHandler):
	def post(self):
		self.redirect('/upload_result?msg=No+news+in+data+store+with+url')
		for upload in self.get_uploads():
			file_name = self.request.get('file_name')
			url = self.request.get('url')
			news_url = self.request.get('news_url')
			#Query parent NewsItem
			news_items = NewsItem.all().filter("url =", news_url).fetch(1)
			#TODO: write out jason for better client response
			if news_items:
				url_list = [image.file_name for image in news_items[0].images]
				if url in url_list:
					self.redirect('/upload_result?msg=Image+%s+is already uploaded' % url)
				else:
					#Create new image
					image = Image( news_item = news_items[0], 
									url = url, 
									file_name = file_name, 
									blob_key = str(upload.key())
								)
					image.put()
					self.redirect('/upload_result?msg=Image+%s+is+uploaded+with+key+%s' % (url, image.blob_key))
			else:
				self.redirect('/upload_result?msg=No+news+in+data+store+with+url+%s' % news_url)

class UploadResult(webapp.RequestHandler):
	def get(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		if 'msg' in values:
			self.response.out.write(values.get('msg'))
		else:
			self.response.out.write('No message returned')
					
class ImagePath(webapp.RequestHandler):
	def get(self):
		upload_url = blobstore.create_upload_url('/upload_news_images')
		self.response.out.write(upload_url)
	
class DeleteAll(webapp.RequestHandler):
	def get(self):
		for news in NewsItem.all():
			news.delete()
		for image in Image.all():
			image.delete()
		for blob in  BlobInfo.all():
			blob.delete()
		self.response.out.write("All data and blobs are deleted")

class ImageDownloadHandler(blobstore_handlers.BlobstoreDownloadHandler):
	def get(self,resource):
		file_name = str(urllib.unquote(resource))
		self.response.out.write(file_name)
		images = Image.all().filter('file_name =', file_name).fetch(1)
		blob_key = images[0].blob_key if images else None
		if blob_key and blobstore.get(blob_key):
			self.send_blob(blob_key)
		else:
			self.error(404)

application = webapp.WSGIApplication([
	('/', MainPage),
	('/post_news', PostNews),
	('/get_topics', Topics),
	('/get_news_headers', NewsHeader),
	('/get_news_content', NewsContent),
	('/upload_news_images', Images),
	('/get_image_upload_path', ImagePath),
	('/upload_result', UploadResult),
	('/delete_all', DeleteAll),
	('/images/(.*)',ImageDownloadHandler),
	('/test',Test)
], debug=True)


def main():
	wsgiref.handlers.CGIHandler().run(application)

if __name__ == '__main__':
	main()
