#!/usr/bin/env python

import cgi
import datetime
import wsgiref.handlers

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext import webapp

from django.utils import simplejson

class NewsItem(db.Model):
	topic_id = db.IntegerProperty()
	url = db.StringProperty()
	title = db.StringProperty()
	description = db.TextProperty()
	content = db.TextProperty()
	images = db.StringProperty(multiline=True)
	publish_date = db.DateTimeProperty()
	sync_date = db.DateTimeProperty(auto_now_add=True)


class MainPage(webapp.RequestHandler):
	def get(self):
		self.response.out.write('Welcome to MBC news gateway!')

class PostNews(webapp.RequestHandler):
	def post(self):
		if 'url' in self.request.arguments():
			def update_news():
				values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
				url = self.request.get('url')
				item = NewsItem.get_by_key_name(url)
				if item is None:
					item = NewsItem(key_name = url)
					item.topic_id = 1 #Xa hoi
					item.url = url
					item.title = values.get('title','')
					item.description = db.Text(values.get('description',''))
					item.content = db.Text(values.get('content',''))
					item.images = values.get('images','')
					item.publish_date = datetime.datetime.strptime(self.request.get('publish_date'),'%m/%d/%Y %H:%M:%S') if 'publish_date' in values else None
				item.put()
				self.response.out.write(url)
			db.run_in_transaction(update_news)
		else:
			self.response.out.write('No url posted.')
	def get(self):
		self.response.out.write('No item posted.')
		
class Topics(webapp.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'application/json'
		json_data = [{'id':0,'topic':'Trang Nhat'},{'id':1,'topic':'Xa Hoi'},{'id':2,'topic':'Kinh Te'}]
		self.response.out.write(simplejson.dumps(json_data))
		
class NewsHeader(webapp.RequestHandler):
	def get(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		topic_id = int(values.get('topic_id',0))
		offset = int(values.get('offset',0))
		limit = int(values.get('limit',10))
		if limit > 20:
			limit = 20
		elif limit < 0:
			limit = 10
		if offset < 0:
			offset = 0
		
		items = NewsItem.all().filter("topic_id =",topic_id).order("-sync_date").fetch(offset=offset,limit=limit)
		json_data = [{'news_id':item.url,'news_title':item.title,'news_description':item.description} for item in items]
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(simplejson.dumps(json_data))

class NewsContent(webapp.RequestHandler):
	def get(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		url = values.get('news_id','xxxxxxxxxxxxxx')
		item = NewsItem.get_by_key_name(url)
		json_data = {'news_id':item.url,'news_title':item.title,'news_content':item.content,'news_date':item.publish_date,'news_image_url':item.images} 
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(simplejson.dumps(json_data))
							
		
application = webapp.WSGIApplication([
	('/', MainPage),
	('/post_news', PostNews),
	('/get_topics', Topics),
	('/get_news_headers', NewsHeader),
	('/get_news_content', NewsContent)
], debug=True)


def main():
	wsgiref.handlers.CGIHandler().run(application)

if __name__ == '__main__':
	main()
